package co.dkatalis.atm;

public class Utils {
	public static double objectToDouble(Object object) {
		double result = 0.000;
		try{
			result = Double.parseDouble(object.toString());
		}catch (Exception e) {
			result = 0.00;
		}
		return result;
	}
	
	public static String objectToString(Object object) {
		String result;
		try{
			result = object.toString();
		}catch (Exception e) {
			result = "";
		}
		if(object == null) {
			result = "";
		}
		return result;
	}
}
