package co.dkatalis.atm;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Logger;

public class Core {
	public static void main(String[] args) {
		Properties properties 	= getProperties();
		String usernameProp 	= Utils.objectToString(properties.get("username"));
		String amountProp 		= Utils.objectToString(properties.get("amount"));
		String commandProp 		= Utils.objectToString(properties.get("command"));
		Boolean logged 			= false;
		String currentUser		= "";
		Double currentAmount	= 0.00;
		int currentindex		= 0;
		
		ArrayList<String> listCommand 	= new ArrayList<String>(Arrays.asList(commandProp.split(",")));
		ArrayList<String> listAmount 	= new ArrayList<String>(Arrays.asList(amountProp.split(",")));
		ArrayList<String> listUsername = new ArrayList<String>(Arrays.asList(usernameProp.toLowerCase().split(",")));
		System.out.println("Registered Username : "+listUsername);
		
		while(true) {
			@SuppressWarnings("resource")
			Scanner input 	= new Scanner(System.in);
	        String inputText= input.nextLine();
	        String[] text	= inputText.split(" ");
	        String command 	= text[0].toLowerCase();
	        if(!listCommand.contains(command)) {
	        	System.out.println("Command Not Found");
	        	continue;
	        }
	        
	        if(command.equalsIgnoreCase("logout")) {
	        	if(logged) {
	        		System.out.println("Goodbye, "+currentUser);
	        		logged 			= false;
	        		currentUser		= "";
	        		currentAmount	= 0.00;
	        		currentindex	= 0;
	        	}else {
	        		System.out.println("Login First");
	        	}
	        	continue;
	        }
	        
	        if(text.length < 2) {
	        	System.out.println("Command Not Complete");
	        	continue;
	        }
	        
	        switch(command) {
	        	case "login":
	        		if(logged) {
	        			System.out.println("Logout First");
	        			continue;
	        		}
	        		
	        		String name =	text[1].toLowerCase();
	        		if(!listUsername.contains(name)) {
	        			listUsername.add(name);
	        			listAmount.add("0.00");
	        		}
	        		
	        		currentindex	= listUsername.indexOf(name);
	        		currentAmount 	= Utils.objectToDouble(listAmount.get(currentindex));
	        		currentUser		= name;
	        		logged			= true;
	        		System.out.println("Hello, "+currentUser+"!");
	        		System.out.println("Your balance is $"+currentAmount);
	        	break;
	        	case "deposit":
	        		if(!logged) {
	        			System.out.println("Login First");
	        			continue;
	        		}
	        		
	        		double deposit	= Utils.objectToDouble(text[1]);
	        		currentAmount	= currentAmount + deposit;
	        		listAmount.set(currentindex, Utils.objectToString(currentAmount));
	        		System.out.println("Your balance is $"+currentAmount);
	        	break;
	        	case "withdraw":
	        		if(!logged) {
	        			System.out.println("Login First");
	        			continue;
	        		}
	        		
	        		double withdraw = Utils.objectToDouble(text[1]);
	        		if(withdraw > currentAmount) {
	        			System.out.println("Insufficient Balance");
	        			continue;
	        		}
	        		
	        		currentAmount	= currentAmount - withdraw ;
	        		listAmount.set(currentindex, Utils.objectToString(currentAmount));
	        		System.out.println("Your balance is $"+currentAmount);
	        	break;
	        	case "transfer":
	        		if(!logged) {
	        			System.out.println("Login First");
	        			continue;
	        		}
	        		
	        		if(text.length < 3) {
	    	        	System.out.println("Command Not Complete");
	    	        	continue;
	    	        }
	        		
	        		String target =	text[1].toLowerCase();
	        		if(!listUsername.contains(target)) {
	        			System.out.println("Target Not Found");
	        			continue;
	        		}
	        		
	        		double transfer = Utils.objectToDouble(text[2]);
	        		if(transfer > currentAmount) {
	        			System.out.println("Insufficient Balance");
	        			continue;
	        		}
	        		
	        		int targetIndex		= listUsername.indexOf(target);
	        		double targetAmount = Utils.objectToDouble(listAmount.get(targetIndex));
	        		targetAmount	= targetAmount + transfer;
	        		listAmount.set(targetIndex, Utils.objectToString(targetAmount));
	        		
	        		currentAmount	= currentAmount - transfer ;
	        		listAmount.set(currentindex, Utils.objectToString(currentAmount));
	        		System.out.println("Transferred $"+transfer+" to "+target);
	        		System.out.println("Your balance is $"+currentAmount);
	        	break;
	        default:
	      }
		}
	}
	
	public static Properties getProperties() {
		Properties properties = new Properties();
		String propertiesFile = "config.properties";
		ClassLoader loader = Thread.currentThread().getContextClassLoader(); 
		InputStream inputStream = null;
		try {
			inputStream = loader.getResourceAsStream(propertiesFile);
			if (inputStream != null) {
				properties.load(inputStream);
			}
		}catch (Exception e) {
			Logger.getLogger(e.getMessage());
		}
		return properties;
	}
}
